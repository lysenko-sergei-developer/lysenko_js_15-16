'use stict';
// my api key 4299403-cbc1ac608a22453880018c940

ajaxModule = function() {};
ajaxModule.prototype = {

	searchButtonClick: function() {

		let btn = document.getElementById('search__btn');

		btn.onclick = function(e) {

      let keyword =  document.getElementsByClassName('search__field')[0].value.toLowerCase();
      keyword = encodeURIComponent(keyword.trim());

  		newAjaxModule.ajaxParse(keyword);
		}
	},

	ajaxParse: function(request, callback) {
		var self = this;
		var API_KEY = '4299403-cbc1ac608a22453880018c940';
		let username = 'yamaha001001';

    let url = 'https://pixabay.com/api/videos/?username=' + username +
      '&key=' + API_KEY + '&q=' + request;

    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        var resp = request.responseText;

        var myObject = eval('(' + resp + ')');

        self.ajaxProcess(myObject);
      } else {
        console.log(response);
      }
    };

    request.onerror = function() {
      console.log(response);
    };

    request.send()
	},

	ajaxProcess: function(requestData, callback) {

    var video = document.getElementById('video__list');
    var arrVideo = document.querySelectorAll('.video');


		if (arrVideo) {
			this.removeElements(arrVideo);
		}

    let data = requestData.hits;

    data.forEach.call(requestData.hits, function(value, index) {
      var elemVideo = document.createElement('video');
      elemVideo.className = 'video video' + index;

      elemVideo.controls = 'controls';

      if (value.userImageURL == "")
        elemVideo.poster = "https://pbs.twimg.com/media/CtqIQEFUIAAVlKO.jpg";
      else
        elemVideo.poster = value.userImageURL;

      elemVideo.style.width = '400px';
      elemVideo.style.height = '400px';

      video.insertBefore(elemVideo, video.firstChild);

      var elemSource = document.createElement('source');
      elemSource.src = value.videos.small.url;

      elemVideo.appendChild(elemSource);
    });
	},

	removeElements: function(arrVideo, callback) {
		for (let i = 0; i < arrVideo.length; i++) {
			let nextElem = arrVideo[i];
			nextElem.parentNode.removeChild(nextElem);
		}
	},
}

newAjaxModule = new ajaxModule();
newAjaxModule.searchButtonClick();

var Human = function(){};
Human.prototype = {
  name: "",
  year: "",
  gender: "",
  height: "",
  weight: ""
};

var Worker = function(){};
Worker.prototype = {
  workPlace: "",
  salary: "",

  work: function() {
    console.log('i work in ' + this.workPlace);
    console.log('for ' + this.salary);
  },

  Worker: function(name, year, gender, height, weight, workPlace, salary) {
    this.name = name;
    this.year = year;
    this.gender = gender;
    this.height = height;
    this.weight = weight;
    this.workPlace = workPlace;
    this.salary = salary;
  }

};

var Student = function(){};
Student.prototype = {
  studyPlace: "",
  grants: "",

  watchSerial: function() {
    console.log('i don\'t do anything, but i like a money');
    console.log('now i watch \"Wallking Dead\"');
  },

  Student: function(name, year, gender, height, weight, studyPlace, grants) {
    this.name = name;
    this.year = year;
    this.gender = gender;
    this.height = height;
    this.weight = weight;
    this.studyPlace = studyPlace;
    this.grants = grants;
  }
}

Worker.prototype.__proto__ = Human.prototype;

var workerPetr = new Worker();
var workerOleg = new Worker();

workerPetr.Worker('Petro', '25', 'male', '180', '78', 'EPAM', '3800');
workerOleg.Worker('Oleg', '19', 'male', '178', '66', 'LightSoftDay', '300');

console.log('----------------');

Student.prototype.__proto__ = Human.prototype;

var studentAnton = new Student();
var studentKarina = new Student();

studentAnton.Student('Anton', '21', 'male', '180', '76', 'KPI', '$$$');
studentKarina.Student('Karina', '22', 'female', '168', '52', 'Shevchenka', '1000');


console.log(workerPetr);
console.log(workerOleg);
console.log(studentAnton);
console.log(studentKarina);

workerPetr.work();
workerOleg.work();
studentAnton.watchSerial();
studentKarina.watchSerial();
